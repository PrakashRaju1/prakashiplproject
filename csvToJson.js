const csv = require('csvtojson');
const fs = require('fs');
const path = require('path');

const csvFilePath1 = path.resolve(__dirname,'./src/data/matches.csv');
const csvFilePath2 = path.resolve(__dirname,'./src/data/deliveries.csv');

const jsonFilePath1 = path.resolve(__dirname,'./src/data/matches.json');
const jsonFilePath2 = path.resolve(__dirname,'./src/data/deliveries.json');


function csvToJson(filePath,fileName){
    csv()
    .fromFile(filePath)
    .then((json)=>{  
    fs.writeFile(fileName, JSON.stringify(json), (err) => { 
        if (err) console.log(err)
        //console.log(json);
    }) 
})
    
}

csvToJson(csvFilePath1,jsonFilePath1);
csvToJson(csvFilePath2,jsonFilePath2);


