let matches = require('../data/matches.json');
let deliveries = require('../data/deliveries.json');
const fs = require('fs');


//Number of matches played per year  in IPL.
let matchesPerYear= matches=>{
  let iplMatchesPerYear = matches.reduce((iplMatchesPerYear, currentMatch) => {
    if (iplMatchesPerYear.hasOwnProperty(currentMatch.season)) {
      iplMatchesPerYear[currentMatch.season] += 1;
    } else {
      iplMatchesPerYear[currentMatch.season] = 1;
      //console.log(iplMatchesPerYear[currentMatch.season]);
    }
    return iplMatchesPerYear;
  }, {})
  return iplMatchesPerYear;
}
//let result = matchesPerYear(matches);
// console.log(result);

//Number of matches won per team per year in IPL.
let  matchesWonPerYear=matches => {
  let iplMatcheResults = matches.reduce((iplMatcheResults, currentMatch) => {
    if (iplMatcheResults.hasOwnProperty(currentMatch.season)) {
      if (iplMatcheResults[currentMatch.season].hasOwnProperty(currentMatch.winner)) {
        iplMatcheResults[currentMatch.season][currentMatch.winner] += 1;
        //console.log(iplMatcheResults[currentMatch.season]);
      } else {
          if (currentMatch.winner != ''){
            iplMatcheResults[currentMatch.season][currentMatch.winner] = 1;
            //console.log(iplMatcheResults[currentMatch.season]);
          }
        }
    }
    else {
      iplMatcheResults[currentMatch.season] = {};
      //console.log(iplMatcheResults[currentMatch.season]);
    }
    return iplMatcheResults;
  }, {});

  return iplMatcheResults;
}

//const result = matchesWonPerYear(matches);

//console.log(result);

//Extra runs conceded per team in the year 2016
let extraRunsInYear2016=(deliveries, matches) =>{

  let getIdOf2016 = matches.filter((match) => match.season == 2016)
    .map((match) => parseInt(match.id));

  let extraRunsIn2016 = deliveries.reduce((extraRunsIn2016, currentBall) => {
    let matchId = parseInt(currentBall.match_id);
    if (getIdOf2016.includes(matchId)) {
      if (extraRunsIn2016.hasOwnProperty(currentBall.bowling_team)) {
        extraRunsIn2016[currentBall.bowling_team] += parseInt(currentBall.extra_runs);
      } else {
        extraRunsIn2016[currentBall.bowling_team] = parseInt(currentBall.extra_runs);
      }
    }
    return extraRunsIn2016;
  }, {})
  return extraRunsIn2016;
}

//const result = extraRunsInYear2016(deliveries, matches);
//console.log(result);


//Top 10 economical bowlers in the year 2015

let topTenEconomicalBowlers=(deliveries, matches) =>{

  let getIdOf2015 = matches.filter((match) => match.season == 2015)
    .map((match) => parseInt(match.id));

  let allBowlers = deliveries.reduce((allBowlers, currentBall) => {

    let matchId = parseInt(currentBall.match_id)
    if (getIdOf2015.includes(matchId)) {
      if (allBowlers.hasOwnProperty(currentBall.bowler)) {
        allBowlers[currentBall.bowler].balls += 1;
        allBowlers[currentBall.bowler].runs += parseInt(currentBall.total_runs);
        let run = allBowlers[currentBall.bowler].runs;
        let ball = allBowlers[currentBall.bowler].balls;
        allBowlers[currentBall.bowler].economy = (run / (ball / 6)).toFixed(2);
      } else {
        allBowlers[currentBall.bowler] = { "runs": parseInt(currentBall.total_runs), "balls": 1, "economy": 0 };
      }
    }
    return allBowlers;
  }, {})
  
  const getEconomy = [];
  for(let bowlerName in allBowlers){
    if(allBowlers[bowlerName].economy <10 ){
      getEconomy.push(allBowlers[bowlerName].economy);
      }
  }

  getEconomy.sort();
  //console.log(getEconomy);
  const top10EconomyBowlers = {};
  for (let index = 0; index <=8; index++) {
    for (key in allBowlers) {
      if (allBowlers[key].economy == getEconomy[index]) {
        top10EconomyBowlers[key] = { economyRate: allBowlers[key].economy };
      }
    }
  }

  return top10EconomyBowlers;

}

 //const result = topTenEconomicalBowlers(deliveries, matches);
 //console.log(result);


module.exports = {
    matchesPerYear,
    matchesWonPerYear,
    extraRunsInYear2016,
    topTenEconomicalBowlers
  };
  
  