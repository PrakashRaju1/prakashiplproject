const path = require('path');
let matches = require('../data/matches.json');
let deliveries = require('../data/deliveries.json');
const fs = require('fs');
const ipl = require('./ipl.js');
const noOfTimesWonTossAndMatches =require("../server/noOfTimesWonTossAndMatches.js");
const playerOfTheMatch=require("../server/playerOfTheMatch.js");
const superOverEconomy=require("../server/superOverEconomy");

const iplMatchesPerYear = ipl.matchesPerYear(matches);
filePath1 = path.resolve(__dirname,"../public/output/matchesPerYear.json");
writeJsonObj(filePath1,iplMatchesPerYear);

const iplWonPerYear= ipl.matchesWonPerYear(matches);
filePath2 = path.resolve(__dirname,"../public/output/matchesWonPerYear.json");
writeJsonObj(filePath2,iplWonPerYear);

const extraRunsIn2016 = ipl.extraRunsInYear2016(deliveries, matches)
filePath3 = path.resolve(__dirname,"../public/output/extraRunsInYear2016.json");
writeJsonObj(filePath3,extraRunsIn2016);


const topEconomyBowlersIpl = ipl.topTenEconomicalBowlers(deliveries, matches);
filePath4 = path.resolve(__dirname,"../public/output/topTenEconomicalBowlers.json");
writeJsonObj(filePath4,topEconomyBowlersIpl);

const wonTossAndMatches = noOfTimesWonTossAndMatches(matches);
filePath5 = path.resolve(__dirname,"../public/output/noOfTimesWonTossAndMatches.json");
writeJsonObj(filePath5,wonTossAndMatches );

const getPlayerOfTheMatch= playerOfTheMatch(matches);
filePath6 = path.resolve(__dirname,"../public/output/playerOfTheMatch.json");
writeJsonObj(filePath6,getPlayerOfTheMatch );

const superOverBestEconomy = superOverEconomy(deliveries);
filePath7 = path.resolve(__dirname,"../public/output/superOverEconomy.json");
writeJsonObj(filePath7,superOverBestEconomy);




function writeJsonObj(filePath,FileName){
    fs.writeFile(filePath, JSON.stringify(FileName), (err) => {
       if (err) console.log(err)
    })
    }
    
    
