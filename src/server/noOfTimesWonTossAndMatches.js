//Find the number of times each team won the toss and also won the match
let noOfTimesWonTossAndMatches= matches => {
  
    let getWonTossAndMatches = matches.reduce((getWonTossAndMatches, currentMatch) => {
      if (getWonTossAndMatches.hasOwnProperty(currentMatch.season)) {
        if (currentMatch.toss_winner == currentMatch.winner) {
          getWonTossAndMatches[currentMatch.season] += 1;
        }
      }else{
        if (currentMatch.toss_winner == currentMatch.winner) {
        getWonTossAndMatches[currentMatch.season] = 1;
        }else{ 
        getWonTossAndMatches[currentMatch.season] = 0;
        }
      }
      return getWonTossAndMatches;
    }, {});
  
    return getWonTossAndMatches;
  }
  
  //const result = noOfTimesWonTossAndMatches(matches);
  //console.log(result);

  module.exports=noOfTimesWonTossAndMatches;