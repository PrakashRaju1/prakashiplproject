
let matches = require('../data/matches.json');
let deliveries = require('../data/deliveries.json');
//Find the bowler with the best economy in super overs
let superOverEconomy=deliveries =>{

    let economyBowlers = deliveries.reduce((economyBowlers, match) => {
  
      if (economyBowlers.hasOwnProperty(match.bowler)) {
  
        if (match.is_super_over == '1') {
          economyBowlers[match.bowler].balls += 1;
          economyBowlers[match.bowler].runs += parseInt(match.total_runs);
          let run = economyBowlers[match.bowler].runs;
          let ball = economyBowlers[match.bowler].balls;
          economyBowlers[match.bowler].economy = (run / (ball / 6)).toFixed(2);
        }
      }
      else {
        if (match.is_super_over == '1') {
          economyBowlers[match.bowler] = { "runs": parseInt(match.total_runs), "balls": 1, "economy": 0 };
        }
      }
      return economyBowlers;
    }, {})
  
  
    let economyOnly = Object.values(economyBowlers).map((valuesOfEconomyBowlers) => parseFloat(valuesOfEconomyBowlers.economy));
   
    const economyOfTop = economyOnly.filter((economyOfBowler) => {
      if (economyOfBowler < 10) {
        return economyOfBowler;
      }
    })
    economyOfTop.sort()
  
    const topEconomy = {};
    for (let key in economyBowlers) {
      if (economyBowlers[key]["economy"] == economyOfTop[0]) {
        topEconomy[key] = economyOfTop[0];
      }
    }
    return topEconomy;
  }
  
  //const superOverBestEconomy = superOverEconomy(deliveries);

  module.exports=superOverEconomy;