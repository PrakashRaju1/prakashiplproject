//Find a player who has won the highest number of Player of the Match awards for each season
let playerOfTheMatch=matches =>{

    let getManOfTheMatches = matches.reduce((getManOfTheMatches, match) => {
      if (getManOfTheMatches.hasOwnProperty(match.season)) {
  
        if (getManOfTheMatches[match.season].hasOwnProperty(match.player_of_match)) {
          getManOfTheMatches[match.season][match.player_of_match] += 1;
        } else {
          getManOfTheMatches[match.season][match.player_of_match] = 1;
        }
      }
      else {
        getManOfTheMatches[match.season] = {};
      }
      return getManOfTheMatches;
    }, {});
    
    let sortedOrderOfAwards = Object.values(getManOfTheMatches)
        .map(nameAndNoOfAwards => Object.entries(nameAndNoOfAwards)
        .sort((current, previous) => current[1] - previous[1]));
  
    const topPlayers = sortedOrderOfAwards.map(nameAwards => nameAwards[nameAwards.length-1]);
   
    let mostAwardsPerYear =  matches.reduce((mostAwardsPerYear, match) => {
      mostAwardsPerYear[match.season] = 0;
      return mostAwardsPerYear;
  }, {})
  
    
    Object.keys(mostAwardsPerYear).forEach((element,index) => {
      let name = topPlayers[index][0];
      let value = topPlayers[index][1];
      mostAwardsPerYear[element] = { [name]: value }
    })
   
    return mostAwardsPerYear;
  }
  
  //const result = playerOfTheMatch(matches);
  //console.log(result)
module.exports=playerOfTheMatch;